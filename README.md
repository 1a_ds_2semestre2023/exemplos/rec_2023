# Projeto - Rec 2023

<div align="center">
[![NodeJs](https://img.shields.io/badge/devel-Node-brightgreen)]
</div>
Este é um projeto de exemplo para demonstrar uma API simples de phones. Siga as instruções abaixo para clonar e iniciar o projeto.

## Clonando o Projeto

Abra o terminal e execute o seguinte comando para clonar o repositório:

```bash
git clone https://gitlab.com/1a_ds_2semestre2023/exemplos/rec_2023.git
```

## Iniciando o Projeto
- Instalando Dependências:
Entre no diretório do projeto e instale as dependências utilizando o comando npm:

```bash
cd rec_2023
npm install
```

## Iniciando a API
- Após instalar as dependências, você pode iniciar a API com o seguinte comando:
```bash
npm start
```
- A API estará disponível em http://localhost:5000. Certifique-se de que a porta 5000 esteja livre (NÃO RODAR 2 API'S).

## Rotas Disponíveis
- GET /rec/api/v1/phones: Retorna a lista de phone.
- GET /health: Verifica a saúde da API.

#### OBS: A rota que retorna a lista de phones você deve criar no arquivo apiRoutes.js corretamente
