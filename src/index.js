const express = require('express')
const app = express()
const cors = require('cors');

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
      this.express.use(cors())
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes')
      this.express.use('/rec/api/v1/', apiRoutes);
      this.express.get('/health/', (_,res)=> {
        res.send({message:'BEM VINDO(A) RECUPERAÇÃO'})
      });
    }
  }

  module.exports = new AppController().express;