const getPhoneList  = require('../services/phones');

module.exports = class controller {
  // Listar aparelhos celulares
  static async getPhones(req, res) {

    // Obtém a lista de telefones
    const phones = await getPhoneList();

    // Console para exibir a lista de Phones no terminal
    console.log(phones)

    // Retorna a lista como resposta
    res.status(200).json({ Phones: phones });
  }

};
